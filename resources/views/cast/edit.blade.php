@extends('layouts.master')

@section('title')
    Edit Cast {{$cast->nama}}
@endsection
@section('content')
    <form action="/cast/{{$cast->id}}" method="POST">
        @csrf
        @method('put')
            <div class="form-group">
                <label >nama</label>
                <input type="text" class="form-control" name="nama" value="{{$cast->nama}}" placeholder="Masukkan Nama Cast">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>umur</label>
                <input type="text" class="form-control" name="umur" value="{{$cast->umur}}" placeholder="Masukkan Umur Cast">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>bio</label>
                <textarea  name="bio" class="form-control" cols="30" rows="10">{{$cast->bio}}"</textarea>
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
        <button type="submit" class="btn btn-primary">Tambah</button>
    </form>
@endsection